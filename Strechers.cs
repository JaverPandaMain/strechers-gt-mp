﻿#region Copyright (C) 2018 BambooPlugins
// <copyright file="Strechers.cs" company="BambooPlugins">
//
// Copyright (C) 2018 BambooPlugins
//
// This program is free software: you can redistribute it and/or modify
// it under the +terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/. 
// </copyright>
// <summary>
// Author: JaverPanda
// Email: JaverPanda@bambooplugins.com
// </summary>
#endregion

using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;

namespace stretchers
{
    public class Strechers : Script
    {
        #region Attributes

        List<NetHandle> _camillasAgarrables = new List<NetHandle>();
        List<NetHandle> _camillasAgarradas = new List<NetHandle>();
        Dictionary<Client, NetHandle> _camillasConPaciente = new Dictionary<Client, NetHandle>();
        const int OBJECTRANGE = 2;
        const int AMBULANCERANGE = 5;
        const int AMBULANCE = 1171614426;
        const int STRECHERMODEL = 272384846;

        #endregion

        #region Constructors

        public Strechers()
        {
            API.onResourceStart += StrechersStart;
            API.onPlayerDisconnected += API_onPlayerDisconnected;
        }

        private void API_onPlayerDisconnected(Client player, string reason)
        {
            try
            {
                var playerPos = API.getEntityPosition(player);
                foreach (Client paciente in _camillasConPaciente.Keys)
                {
                    if (player == paciente)
                    {
                        API.detachEntity(player);
                        NetHandle camilla = _camillasConPaciente[player];
                        _camillasConPaciente.Remove(player);
                        API.deleteEntity(camilla);
                    }
                }

                foreach (NetHandle camillasAgarrada in _camillasAgarradas)
                {
                    foreach (var camilla in _camillasAgarradas)
                        if (isInRange(playerPos, camilla, OBJECTRANGE))
                        {
                            API.detachEntity(player);
                            _camillasAgarradas.Remove(camilla);
                            API.deleteEntity(camilla);
                        }
                }
            }
            catch (Exception)
            {
                //No se hace nada.
            }
        }

        #endregion

        #region Event Handlers
        public void StrechersStart()
        {
        }

        #endregion

        #region Public Methods

        [Command("SacarCamilla")]
        public void SacarCamilla(Client player)
        {
            Vector3 playerPos = API.getEntityPosition(player);
          
                foreach (var vehicle in API.getAllVehicles())
                {

                    if (isInRange(playerPos, vehicle, AMBULANCERANGE))
                    {
                        if (API.getEntityModel(vehicle) == AMBULANCE)
                        {
                        var spawnPos = new Vector3(player.position.X, player.position.Y, player.position.Z - 1);
                        var camilla = API.createObject(STRECHERMODEL, spawnPos, new Vector3(0, 0, 0), 0);
                        API.attachEntityToEntity(camilla, player, "4089", new Vector3(1.2, 0, 0), new Vector3(-10, 187, 260));
                        API.playPlayerAnimation(player, (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl), "anim@mp_ferris_wheel", "idle_a_player_one");
                        _camillasAgarradas.Add(camilla);
                        API.sendNotificationToPlayer(player, "~g~Has sacado una camilla.");
                        return;
                        }
                    }
                }
            API.sendChatMessageToPlayer(player, "~r~Debes estar cerca de una ambulancia.");
           
        }

        [Command("GuardarCamilla")]
        public void GuardarCamilla(Client player)
        {
            Vector3 playerPos = API.getEntityPosition(player);

            foreach (var vehicle in API.getAllVehicles())
            {

                if (isInRange(playerPos, vehicle, AMBULANCERANGE))
                {
                    if (API.getEntityModel(vehicle) == 1171614426)
                    {
                        foreach (var camilla in _camillasAgarradas)
                            if (isInRange(playerPos, camilla, OBJECTRANGE))
                            {
                                API.detachEntity(player);
                                API.stopPlayerAnimation(player);
                                API.deleteEntity(camilla);
                                _camillasAgarradas.Remove(camilla);
                                return;
                            }
                            else
                            {
                                API.sendChatMessageToPlayer(player, "~r~No hay camillas cercanas.");
                            }
                    }
                }
                API.sendChatMessageToPlayer(player, "~r~Debes estar cerca de una ambulancia o no llevas camilla.");
            }
        }//end of AgarrarCamilla

        [Command("AgarrarCamilla")]
        public void AgarrarCamilla(Client player)
        {
            Vector3 playerPos = API.getEntityPosition(player);
            foreach (var camilla in _camillasAgarrables)
            {
                if (isInRange(playerPos, camilla, OBJECTRANGE))
                {
                    API.attachEntityToEntity(camilla, player, "4089", new Vector3(1.2, 0, 0), new Vector3(-10, 187, 260));
                    API.playPlayerAnimation(player, (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl), "anim@mp_ferris_wheel", "idle_a_player_one");
                    _camillasAgarrables.Remove(camilla);
                    _camillasAgarradas.Add(camilla);
                    return;
                }
                else
                {
                    API.sendChatMessageToPlayer(player, "~R~No hay camillas cercanas.");
                }
            }
        }//end of AgarrarCamilla

        NetHandle camilla;

        [Command("SubirPaciente")]
        public void SubirPaciente(Client player, Client target)
        {
            Vector3 playerPos = API.getEntityPosition(player);
            foreach (var camilla in _camillasAgarradas)
                if (isInRange(API.getEntityPosition(target), camilla, OBJECTRANGE))
                {
                    API.detachEntity(camilla);
                    Vector3 camillaPos = API.getEntityPosition(camilla);
                    API.setEntityPosition(camilla, new Vector3(camillaPos.X, camillaPos.Y, camillaPos.Z - 1));
                    API.stopPlayerAnimation(player);
                    API.playPlayerAnimation(player, (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl), "anim@mp_ferris_wheel", "idle_a_player_one");
                    API.playPlayerAnimation(target, (int)(AnimationFlags.Loop), "chinese_1_int-5", "cs_russiandrunk_dual-5");
                    API.attachEntityToEntity(target, player, "6286", new Vector3(1.1, 0.2, 0.1), new Vector3(0, 0, -90));
                    API.attachEntityToEntity(camilla, target, "0", new Vector3(0, -0.4, 0), new Vector3(-90, 0, 0));
                    this.camilla = camilla;
                    _camillasAgarradas.Remove(camilla);
                    _camillasConPaciente.Add(target, camilla);
                    return;
                }
            API.sendChatMessageToPlayer(player, "~R~Debes estar cerca del pj o no tienes una camilla agarrada");
        }

        [Command("MeterAmbulancia")]
        public void MeterAmbulancia(Client player)
        {
            Vector3 playerPos = API.getEntityPosition(player);
            foreach(Client target in _camillasConPaciente.Keys)
            {
                foreach (var vehicle in API.getAllVehicles()){
                 
                    if (isInRange(playerPos, vehicle, AMBULANCERANGE))
                    {
                        if(API.getEntityModel(vehicle) == AMBULANCE)
                        {
                            API.detachEntity(target);
                            API.detachEntity(camilla);
                            API.deleteEntity(camilla);
                            API.stopPlayerAnimation(player);
                            API.stopPlayerAnimation(target);
                            API.setPlayerIntoVehicle(target, vehicle, 2);
                            _camillasConPaciente.Remove(target);
                            return;
                        }
                    }
                }
            }
            API.sendChatMessageToPlayer(player, "~R~Debes estar cerca de una ambulancia.");
        }

        [Command("SoltarCamilla")]
        public void SoltarCamilla(Client player)
        {
            Vector3 playerPos = API.getEntityPosition(player);
            foreach (var camilla in _camillasAgarradas)
                if (isInRange(playerPos, camilla, OBJECTRANGE))
                {
                    API.detachEntity(camilla);
                    Vector3 camillaPos = API.getEntityPosition(camilla);
                    API.setEntityPosition(camilla, new Vector3(camillaPos.X, camillaPos.Y, camillaPos.Z - 1));
                    API.stopPlayerAnimation(player);
                    _camillasAgarrables.Add(camilla);
                    _camillasAgarradas.Remove(camilla);
                    return;
                }
            API.sendChatMessageToPlayer(player, "No llevas ninguna camilla.");
        }

        #endregion

        #region Private Methods

        private bool isInRange(Vector3 playerPos, NetHandle camilla, int range)
        {
            Vector3 posicionCamilla = API.getEntityPosition(camilla);
            if (posicionCamilla.X - playerPos.X < range && posicionCamilla.X - playerPos.X > -range)
            {
                if (posicionCamilla.Y - playerPos.Y < range && posicionCamilla.Y - playerPos.Y > -range)
                {
                    if (posicionCamilla.Z - playerPos.Z < range && posicionCamilla.Z - playerPos.Z > -range)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
             //end of foreach
        }

        #endregion

        [Flags]
        public enum AnimationFlags
        {
            Loop = 1 << 0,
            StopOnLastFrame = 1 << 1,
            OnlyAnimateUpperBody = 1 << 4,
            AllowPlayerControl = 1 << 5,
            Cancellable = 1 << 7
        }
    }
}